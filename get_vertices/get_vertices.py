import numpy as np
import matplotlib.pyplot as plt
import math
import scipy.misc as sm

def make_circle(n=100):
    dx = 1./np.float(n)
    ci = np.zeros((n,n))
    for i in range(n):
        for j in range(n):
            r = np.sqrt((i*dx-0.5)**2+(j*dx-0.5)**2)
            if  r <= 0.25+dx/2. and r>= 0.25-dx/2.:
                ci[i,j] = 1.0
    return ci

def get_next_vertex(data):
    k,l = data.shape
    point = []
    left    = data[0,:]
    if np.max(left) != 0.:
        point.append([0,np.argmax(left)])
    upper   = data[:,l-1]
    if np.max(upper) != 0.:
        point.append([np.argmax(upper),l-1])
    right   = data[k-1,:]
    if np.max(right) != 0.:
        point.append([k-1,np.argmax(right)])
    lower   = data[:,0]
    if np.max(lower) != 0.:
        point.append([np.argmax(lower),0])
    point = np.array(point)
    x,y = point.shape
    if x!=2 or y!=2:
        if point[0,0] == point[1,0] and point[0,1] == point[1,1]:
            return point[1:]
        else:
            return point
    else:
        return point

def laenge (daten):
    n=len(daten)
    sum=0
    for i in range (0,n):
        if i==n-1:
            dist=math.pow(daten[i][1]-daten[0][1],2)+math.pow(daten[i][0]-daten[0][0],2)
        else:
            dist=math.pow(daten[i][1]-daten[i+1][1],2)+math.pow(daten[i][0]-daten[i+1][0],2)
        distw=math.sqrt(dist)
        sum=sum+distw
    return sum

def get_vertices(data,N):
    k,l = data.shape
    u = np.zeros((1,2),dtype="int32")
    weiter = True
    for i in range(k):
        for j in range(l):
            if data[i,j] != 0. and weiter:
                u[0,:] = [i,j]
                weiter = False
    #print u[0,:]

    last_point = u[0,:]
    i = 0
    while np.sqrt((u[i,0]-u[0,0])**2+(u[i,1]-u[0,1])**2) > N or i < 3:
        temp = data[u[i,0]-N:u[i,0]+N,u[i,1]-N:u[i,1]+N]
        poin = get_next_vertex(temp)

        fst_poin = np.copy(poin[0])
        snd_poin = np.copy(poin[1])

        fst_poin = u[i]+fst_poin-N*np.ones(2,dtype='int32')
        snd_poin = u[i]+snd_poin-N*np.ones(2,dtype='int32')

        fst_r = np.sqrt(np.sum((fst_poin-last_point)**2))
        snd_r = np.sqrt(np.sum((snd_poin-last_point)**2))
        #print fst_r,snd_r

        if fst_r >= snd_r:
            u = np.append(u,[fst_poin],axis=0)
        else:
            u = np.append(u,[snd_poin],axis=0)

        last_point = u[i]
        #print i,fst_poin,snd_poin,u[i+1,:]
        i = i+1

    return u

def main():
    name = "../examples/kreis.png"
    #data = make_circle(n=1000)
    data = np.invert(sm.imread(name,mode='L'))
    k,l = data.shape
    data = np.pad(data,500-k,'constant')
    vl = []
    vx = []

    """
    for n in range(2,70):
        print n
        vertices = get_vertices(data,n)
        vl.append(laenge(vertices))
        k,l = vertices.shape
        vx.append(k)
    #"""
    vertices = get_vertices(data,50)
    np.save(name[:-3]+"npy",vertices)

    print vertices
    k,l = vertices.shape
    print k,l

    f, ax1 = plt.subplots()
    ax1.imshow(data,cmap='gray_r',origin='lower')
    data_t = np.ones_like(data)*0.
    for k in range(k):
        data_t[np.int(vertices[k,0]),np.int(vertices[k,1])] = 1.
    ax1.imshow(data_t,cmap='gray_r',alpha=0.7,origin='lower')
    ax1.grid('on')
    f = plt.figure()
    plt.plot(vertices[:,1],vertices[:,0])
    plt.axis("equal")
    f = plt.figure()
    plt.plot(vl)
    plt.show()


if __name__ == "__main__":
    main()
