import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

def laenge (daten):
    n=len(daten)
    sum=0
    for i in range (0,n):
        if i==n-1:
            dist=math.pow(daten[i][1]-daten[0][1],2)+math.pow(daten[i][0]-daten[0][0],2)
        else:
            dist=math.pow(daten[i][1]-daten[i+1][1],2)+math.pow(daten[i][0]-daten[i+1][0],2)
        distw=math.sqrt(dist)
        sum=sum+distw
    return sum

def get_t(daten,norm=True):
    n,_=daten.shape
    #stvec = np.zeros((n,2))
    stvec = np.zeros_like(daten,dtype='float64')
    for i in range(n):
        if i==0:
            stvec[i]=(daten[0]-daten[-1])
        else:
            stvec[i]=(daten[i]-daten[i-1])
        #"""
        if norm:
            strc = np.sqrt(stvec[i,0]**2+stvec[i,1]**2)
            if strc != 0.:
                stvec[i] = stvec[i]/strc 
        #"""
    return stvec

def ableitung_pt(daten):
    n,_ = daten.shape
    steig=get_t(daten)
    steig_n = get_t(daten,norm=False)
    ska = np.zeros_like(daten,dtype='float64')
    for i in range(n):
        if i==n-1:
            """
            t1 = np.sqrt(np.sum(steig_n[i]**2))
            t2 = np.sqrt(np.sum(steig_n[0]**2))
            ska[i]=-(steig[i]-steig[0])/(t1**2+t2**2)
            #"""
            ska[i]=-(steig[i]-steig[0])
        else:
            """
            t1 = np.sqrt(np.sum(steig_n[i]**2))
            t2 = np.sqrt(np.sum(steig_n[i+1]**2))
            ska[i]=-(steig[i]-steig[i+1])/(t1**2+t2**2)
            #"""
            ska[i]=-(steig[i]-steig[i+1])
    return ska

def center_mass(data):
    n,_ = data.shape
    data_cp = np.copy(data)
    data_cp = data_cp
    cm = np.sum(data_cp,axis=0)/np.float(n)
    return cm

def make_circle(n=10):
    #r = n/2.
    r = 10.
    dx = 1./np.float(n)
    ci = []
    index = np.linspace(0,np.pi,n*2)
    index = np.append(index,np.linspace(np.pi,2.*np.pi,n))
    #for theta in np.linspace(0.,2.*np.pi,n):
    for theta in index:
        i = r*np.cos(theta)+r
        j = r*np.sin(theta)+r
        ci.append([i,j])
    return np.array(ci)

def make_ellipse(n=10):
    r = 10.
    a = 4.
    b = 2.
    dx = 1./np.float(n)
    ci = []
    for theta in np.linspace(0.,2.*np.pi,n):
        i = a*np.cos(theta-np.pi/6.)+r*2.
        j = b*np.sin(theta)+r*2.
        ci.append([i,j])
    return np.array(ci)

def time_step(data,grad):
    n,_ = data.shape
    data_ret = np.zeros_like(data,dtype='float64')
    steig = get_t(data,norm=False)
    steig_r = np.sqrt(np.sum(steig**2,axis=1))
    #dx = np.min(steig_r)/2.
    #print dx
    dx = 0.2
    abl = ableitung_pt(data)
    for i in range(n):
        data_ret[i] = data[i]+abl[i]*dx
    return data_ret

def update_line(num,circl,grad_abl,line,line_cm,line_abl):
    line.set_data(circl[num,:,0],circl[num,:,1])
    cm = center_mass(circl[num])
    line_cm.set_data(cm[0],cm[1])
    #abl = np.roll(get_t(circl[num]),-1,axis=0)
    #abl = ableitung_pt(circl[num])
    line_abl.set_offsets(circl[num,:])
    line_abl.set_UVC(grad_abl[num,:,0],grad_abl[num,:,1],np.sqrt(np.sum(grad_abl[num]**2,axis=1)))
    return line, line_cm, line_abl, 

def main():
    name = "./examples/some_form.npy"
    circl = np.roll(np.load(name),0,axis=0).astype('float64')[:-1]
    #circl = make_ellipse(10)
    #circl = make_circle(10)
    #np.save(name,circl)
    n,_ = circl.shape
    #"""
    xmin = np.min(circl[:,0])
    xmax = np.max(circl[:,0])
    ymin = np.min(circl[:,1])
    ymax = np.max(circl[:,1])
    shift = [(xmax+xmin)/2.,(ymax+ymin)/2.]
    for i in range(n):
        circl[i] = circl[i] - shift
    #"""
        
    circl_1 = []
    grad_abl = []
    circl_1.append(circl)
    grad_abl.append(ableitung_pt(circl))
    ci_len = [laenge(circl)]
    cm_t = [center_mass(circl)]
    steig_n = get_t(circl_1[0],norm=False)
    stgr =np.sqrt(np.sum(steig_n**2,axis=1))
    c_std = [np.std(stgr)/np.mean(stgr)]

    max_it = 6000
    for i in range(1,max_it):
        circl_1.append(time_step(circl_1[i-1],grad_abl[i-1]))
        grad_abl.append(ableitung_pt(circl_1[i]))
        ci_len.append(laenge(circl_1[i]))
        cm_t.append(center_mass(circl_1[i]))
        steig_n = get_t(circl_1[i],norm=False)
        stgr =np.sqrt(np.sum(steig_n**2,axis=1))
        c_std.append(np.std(stgr)/np.mean(stgr))
    grad_abl = np.array(grad_abl)
    circl_1 = np.array(circl_1)
    cm_t = np.array(cm_t)

    FPS = 500.
    #"""
    f,ax1 = plt.subplots()
    plt.plot(circl_1[0,:,0],circl_1[0,:,1],'k-')
    plt.plot(cm_t[0,0],cm_t[0,1],'k+')
    plt.quiver(circl_1[0,:,0],circl_1[0,:,1],grad_abl[0,:,0],grad_abl[0,:,1],np.sqrt(np.sum(grad_abl[0]**2,axis=1)),scale_units='xy',scale=1.)
    line_cm, = plt.plot(cm_t[1,0],cm_t[1,1],'r+')
    line, = plt.plot(circl_1[1,:,0],circl_1[1,:,1],'r-')
    line_abl = plt.quiver(circl_1[1,:,0],circl_1[1,:,1],grad_abl[1,:,0],grad_abl[1,:,1],np.sqrt(np.sum(grad_abl[1]**2,axis=1)),scale_units='xy',scale=1.)
    plt.axis('equal')
    #"""

    line_ani = animation.FuncAnimation(f, update_line, frames=max_it, interval=1./FPS*1000, fargs=(circl_1,grad_abl,line,line_cm,line_abl), blit=False)
    #comment out net line to speed up or if no imagemagick is available
    #line_ani.save("{}.gif".format(name[:-4]), writer='imagemagick', fps=30)
    #line_ani.save("{}.mp4".format(name[:-4]), fps=FPS)

    #"""
    f, (ax1,ax2) = plt.subplots(2,1)
    #ax1.semilogy(ci_len)
    ax1.plot(c_std)
    ax1.set_xlabel('iteration step')
    ax1.set_ylabel('length')
    ax2.plot(cm_t[:,0],cm_t[:,1],'k+-')
    ax2.set_xlabel('x')
    ax2.set_ylabel('y')
    #plt.savefig("{}_lenge.png".format(name[:-4]),bbox='tight',dpi=300)
    #"""
    plt.show()
    

if __name__ == "__main__":
    main()
