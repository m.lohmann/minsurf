import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

def laenge (daten):
    n=len(daten)
    sum=0
    for i in range (0,n):
        if i==n-1:
            dist=math.pow(daten[i][1]-daten[0][1],2)+math.pow(daten[i][0]-daten[0][0],2)
        else:
            dist=math.pow(daten[i][1]-daten[i+1][1],2)+math.pow(daten[i][0]-daten[i+1][0],2)
        distw=math.sqrt(dist)
        sum=sum+distw
    return sum

def get_t(daten):
    n=len(daten)
    stvec = np.zeros((n,2))
    for i in range(0,n):
        if i==0:
            stvec[i,0]=(daten[i][0]-daten[n-1][0])
            stvec[i,1]=(daten[i][1]-daten[n-1][1])
            stvec_r = np.sqrt(stvec[i,0]**2+stvec[i,1]**2)
            if stvec_r != 0.:
                stvec[i] = stvec[i]/stvec_r
        else:
            stvec[i,0]=(daten[i][0]-daten[i-1][0])
            stvec[i,1]=(daten[i][1]-daten[i-1][1])
            stvec_r = np.sqrt(stvec[i,0]**2+stvec[i,1]**2)
            if stvec_r != 0.:
                stvec[i] = stvec[i]/stvec_r
    return stvec

def ableitung_pt(daten):
    n=len(daten)
    sum=0
    steig=list(get_t(daten))
    ska = np.zeros_like(steig)
    for i in range(0,n):
        if i==n-1:
            ska[i]=-(steig[i]-steig[0])
        else:
            ska[i]=-(steig[i]-steig[i+1])
    return ska

def make_circle(n=10):
    #r = n/2.
    r = 10.
    dx = 1./np.float(n)
    ci = []
    for theta in np.linspace(0.,2.*np.pi,n):
        i = r*np.cos(theta)+r
        j = r*np.sin(theta)+r
        ci.append([i,j])
    return np.array(ci)

def time_step(data):
    k,_ = data.shape
    abl = ableitung_pt(data)
    data_ret = np.copy(data)
    for i in range(k):
        data_ret[i] = data[i]+abl[i]/10.
    return data_ret

def update_line(num,circl,line):
    line.set_data(circl[num,:,0],circl[num,:,1])
    return line,


def main():
    name = "./examples/circle.npy"
    #circl = make_circle(n=20)
    #np.save(name,circl)
    circl = np.load(name)
    #circl = np.copy(np.roll(circl,1,axis=1))
    circl_1 = []
    circl_1.append(circl)
    ci_len = [laenge(circl)]

    max_it = 600
    for i in range(1,max_it+1):
        circl_1.append(time_step(circl_1[i-1]))
        ci_len.append(laenge(circl_1[i]))
    circl_1 = np.array(circl_1)

    FPS = 30.
    f,ax1 = plt.subplots()
    line_o = plt.plot(circl_1[0,:,0],circl_1[0,:,1],'r-')
    line, = plt.plot(circl_1[1,:,0],circl_1[1,:,1],'b-')

    line_ani = animation.FuncAnimation(f, update_line, frames=max_it+1, interval=1./FPS*1000, fargs=(circl_1,line), blit=True)
    #comment out net line to speed up or if no imagemagick is available
    #line_ani.save("{}.gif".format(name[:-4]), writer='imagemagick', fps=30)

    f = plt.figure()
    plt.semilogy(ci_len)
    plt.xlabel('iteration step')
    plt.ylabel('length')
    plt.savefig("{}_lenge.png".format(name[:-4]),bbox='tight',dpi=300)
    plt.show()
    

if __name__ == "__main__":
    main()
