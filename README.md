# Minimal Surface

Initial main Programm can be found in root directory of the repository.
It is capable of plotting the time steps for the length minimization.
In the example folder are three different drawn figures (\*.png) and one analytically calculated one.
For the three drawn figures are also three numpy files with some vertices for these.
They have been created with the script get.vertices.py in the folder with the same name.
This script takes a png image and tries to dicretize it with a specified maximum distance.
It is also capable of performing multiple discretizations and plot the resulting length of the frame.
