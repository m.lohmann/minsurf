import math
import numpy as np
import matplotlib.pyplot as plt

def laenge (daten):
    n=len(daten)
    sum=0
    for i in range (0,n):
        if i==n-1:
            dist=math.pow(daten[i][1]-daten[0][1],2)+math.pow(daten[i][0]-daten[0][0],2)
        else:
            dist=math.pow(daten[i][1]-daten[i+1][1],2)+math.pow(daten[i][0]-daten[i+1][0],2)
        distw=math.sqrt(dist)
        sum=sum+distw
    return sum

def steigung (daten):
    n=len(daten)
    #stvec=list(range(n))
    stvec = np.zeros(n)
    for i in range(0,n):
        if i==n-1:
            stvec[i]=(daten[0][1]-daten[i][1])/(daten[0][0]-daten[i][0])
        else:
            stvec[i]=(daten[i+1][1]-daten[i][1])/(daten[i+1][0]-daten[i][0])
    return stvec

def get_t_alt(daten):
    n=len(daten)
    #stvec=list(range(n))
    stvec = np.zeros((n,2))
    for i in range(0,n):
        if i==n-1:
            stvec[i,0]=1.
            stvec[i,1]=(daten[0][1]-daten[i][1])/(daten[0][0]-daten[i][0])
            stvec[i] = stvec[i]/np.sqrt(stvec[i,0]**2+stvec[i,1]**2)
        else:
            stvec[i,0]=1.
            stvec[i,1]=(daten[i+1][1]-daten[i][1])/(daten[i+1][0]-daten[i][0])
            stvec[i] = stvec[i]/np.sqrt(stvec[i,0]**2+stvec[i,1]**2)
    return stvec

def get_t(daten):
    n=len(daten)
    #stvec=list(range(n))
    stvec = np.zeros((n,2))
    for i in range(0,n):
        if i==0:
            stvec[i,0]=(daten[i][0]-daten[n-1][0])
            stvec[i,1]=(daten[i][1]-daten[n-1][1])
            stvec[i] = stvec[i]/np.sqrt(stvec[i,0]**2+stvec[i,1]**2)
        else:
            stvec[i,0]=(daten[i][0]-daten[i-1][0])
            stvec[i,1]=(daten[i][1]-daten[i-1][1])
            stvec[i] = stvec[i]/np.sqrt(stvec[i,0]**2+stvec[i,1]**2)
    return stvec

def ableitung(daten,mu):
    n=len(daten)
    sum=0
    steig=list(steigung(daten))
    for i in range(0,n):
        if i==n-1:
            ska=(steig[i]-steig[0])*mu[i]
        else:
            ska=(steig[i]-steig[i+1])*mu[i]
        sum=sum+ska
    erg=-sum
    return erg

def ableitung_pt(daten):
    n=len(daten)
    sum=0
    steig=list(get_t(daten))
    ska = np.zeros_like(steig)
    for i in range(0,n):
        if i==n-1:
            ska[i]=-(steig[i]-steig[0])
        else:
            ska[i]=-(steig[i]-steig[i+1])
        #sum=sum+ska
    #erg=-sum
    return ska

def make_circle(n=10):
    r = n/2.
    dx = 1./np.float(n)
    ci = []
    #for theta in np.linspace(0.,2.*np.pi-2.*np.pi/np.float(n),n):
    for theta in np.linspace(0.,2.*np.pi,n):
        i = r*np.cos(theta)+r
        j = r*np.sin(theta)+r
        ci.append([i,j])
    return np.array(ci)

def time_step(data):
    n,_ = data.shape
    abl = ableitung_pt(data)
    data_ret = np.copy(data)
    for i in range(n):
        data_ret[i] = data[i]+abl[i]*0.1
    return data_ret
    
d=([1.,2.],[4.,3.],[2.,3.])
#print d
#print(laenge(d))
#print steigung(d)
#print get_t(d)
#abl = ableitung_pt(d)
#print(abl)

circl = make_circle(n=100)
#print circl
#print steigung(circl)
#print get_t(circl)
#print ableitung_pt(circl)
circl_o = np.copy(circl)
ci_len = [laenge(circl)]
for i in range(1100):
    circl_1 = time_step(circl)
    circl = np.copy(circl_1)
    ci_len.append(laenge(circl))
plt.plot(circl_o[:,0],circl_o[:,1],'r-')
plt.plot(circl_1[:,0],circl_1[:,1],'b-')
#plt.plot(ci_len)
plt.show()
